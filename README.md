[![Build Status](https://runbot.odoo.com/runbot/badge/flat/1/master.svg)](https://runbot.odoo.com/runbot)
[![Tech Doc](https://img.shields.io/badge/master-docs-875A7B.svg?style=flat&colorA=8F8F8F)](https://www.odoo.com/documentation/15.0)
[![Help](https://img.shields.io/badge/master-help-875A7B.svg?style=flat&colorA=8F8F8F)](https://www.odoo.com/forum/help-1)
[![Nightly Builds](https://img.shields.io/badge/master-nightly-875A7B.svg?style=flat&colorA=8F8F8F)](https://nightly.odoo.com/)

Eearthbnb
----
Earthbnb is a data management application for businesses that has simmilar requirements with Airbnb (Vacation property rental companies). The main functions is to track properties, users, staff members, transactions, etc.

Code & Feature Description
----
Models:
- Property
- PropertyType
- Features (Bedrooms, Bathrooms, etc.)
- ResPartner (Users: OWners & Renters)
- Staff (Inherited by: Cleaners & Chefs)
- Transactions 

Relations:
- Property m2o PropertyType
- Property m2o Owner
- Property m2m Feature
- Property o2m Transaction
- Transaction m2o Renter

Report:
- XLSX report for property listings
- PDF report for property listings

Wizzard:
- Wage increase by percentage for cleaners

Added after Initial submission: (Postponed)

