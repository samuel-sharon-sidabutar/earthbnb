from odoo import api, fields, models

class WageIncrease(models.TransientModel):
    _name = 'earthbnb.wageincrease'
    
    cleaner_id = fields.Many2one(
        comodel_name='earthbnb.cleaner', 
        string='Cleaner Name',
        required=True
    )

    increase_rate = fields.Integer(
        string='Wage Increase Percentage (10=10%)',
        required=False
    )

    def button_wage_increase(self):
        for rec in self:
            self.env['earthbnb.cleaner']\
                .search([('id', '=', rec.cleaner_id.id)])\
                .write({'hourly_wage': rec.cleaner_id.hourly_wage + int(rec.increase_rate/100*rec.cleaner_id.hourly_wage)})
