from odoo import api, fields, models

class Property(models.Model):
    _name = "earthbnb.property"
    _description = "Propperties"

    name = fields.Char(string='Address')
    cost_per_night = fields.Integer(string='Cost Per Night')
    # daily_price = fields.Integer(string='Daily Price')
    # weekly_price = fields.Integer(string='Weekly Price')
    # monthly_price = fields.Integer(string='Monthly Price')
    description = fields.Char(string='Description')

    owner = fields.Many2one(comodel_name='res.partner', string='Owner', ondelete='cascade')
    type = fields.Many2one(comodel_name='earthbnb.propertytype', string='Property Type', ondelete='cascade')
    features = fields.Many2many(comodel_name='earthbnb.feature', string='Property Features')
    transactions = fields.One2many(comodel_name='earthbnb.transaction', inverse_name='property', string='Transactions')

    _sql_constraints = [
        ('unique_address', 'unique(name)', 'That Address has already been registered')
    ]
