from odoo import api, fields, models

class Feature(models.Model):
    _name = "earthbnb.feature"
    _description = "Property Features (Bedroom, Bathroom, Kitchen, etc."

    name = fields.Char(string='Feature Name')
    ammount = fields.Integer(string='Ammount')    

    position = fields.Selection([
        ('indoor', 'Indoor'),
        ('outdoor', 'Outdoor')
    ], string='Feature Position')

    use_case = fields.Selection([
        ('functional', 'Functional'),
        ('decorative', 'Decorative')
    ], string='Feature use case')

    properties = fields.Many2many(comodel_name='earthbnb.property', string='Properties')
    