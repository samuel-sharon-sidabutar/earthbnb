from odoo import api, fields, models


class Staff(models.Model):
    _name = 'earthbnb.staff'
    _description = 'Staff Member'

    name = fields.Char(string='Name')
    phone_number = fields.Char(string='Phone Number')
    staff_address = fields.Char(string='Address')
    birthday = fields.Date(string='Birthday')
    hourly_wage = fields.Integer(string='Wage Per Hour')
    

class Chef(models.Model):
    _name = 'earthbnb.chef'
    _inherit = 'earthbnb.staff'
    _description = 'Private Chef'

    id_chef = fields.Char(string='ID Chef')

class Cleaner(models.Model):
    _name = "earthbnb.cleaner"
    _inherit = 'earthbnb.staff'
    _description = "Cleaning Person"

    id_cleaner = fields.Char(string='ID Cleaning Service')