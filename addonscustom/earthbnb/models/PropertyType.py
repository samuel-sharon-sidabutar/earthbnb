from odoo import api, fields, models

class PropertyType(models.Model):
    _name = "earthbnb.propertytype"
    _description = "Property types"

    name = fields.Selection([
        ('room', 'Single Room'),
        ('apartment', 'Apartment Unit'),
        ('house', 'House')
    ], string='Property Type')

    properties = fields.One2many(comodel_name='earthbnb.property', inverse_name='type', string='Properties')
    property_ammount = fields.Char(compute='_compute_ammount_item', string='Property Ammount')

    @api.depends('properties')
    def _compute_ammount_item(self):
        for i in self:
            properties_list = self.env['earthbnb.property'].search([('type', '=', i.id)])
            i.property_ammount = len(properties_list)

    