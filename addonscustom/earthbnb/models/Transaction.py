from odoo import api, fields, models
from odoo.fields import datetime
from datetime import timedelta
from odoo.exceptions import ValidationError


class Transaction(models.Model):
    _name = 'earthbnb.transaction'
    _description = 'Earthbnb Transactions'

    name = fields.Char(string='Transaction No.')
    transaction_date = fields.Datetime(string='Transaction Date', default = fields.Datetime.now())
    arrival_date = fields.Date(string='Arrival Date')
    staying_period = fields.Integer(string='Staying Period (days)')
    

    property = fields.Many2one(comodel_name='earthbnb.property', string='Property')
    renter = fields.Many2one(comodel_name='res.partner', string='Renter')
    total_cost = fields.Integer(compute='_compute_total_cost', string='Total Cost (rupiah)')
    
    @api.depends('property')
    def _compute_total_cost(self):
        for i in self:
            reserved_property = self.env['earthbnb.property'].search([('name','=',i.property.name)])
            i.total_cost = int(1.1*reserved_property.cost_per_night*i.staying_period)

    state = fields.Selection(string='State', 
            selection=[('reserved', 'Reserved'),
                        ('booked','Booked'),
                        ('cancelled', 'Cancelled')], 
            required=True, readonly=True, default='reserved')

    def action_reserve(self):
        self.write({'state':'reserved'})    

    def action_book(self):
        self.write({'state':'booked'})    

    def action_cancel(self):
        self.write({'state':'cancelled'})   

    def unlink(self):
        if self.filtered(lambda line: line.state != 'cancelled'):
            raise ValidationError("Please cancel the transaction before deleting it")
        record = super(Transaction,self).unlink() 

    # Constaint to make sure that renters don't input an arrival date while the property is still not available
    @api.constrains('arrival_date')
    def check_property_availability(self):
        for i in self:
            same_property_t = self.env['earthbnb.transaction'].search([('property.name','=',i.property.name)])
            # iterate through previoursly registered transactions 
            for registered_t in same_property_t:
                unaval_date = registered_t.arrival_date
                # Double for loop to verify that no dates overlap
                # Might be simplified by creating lists of the dates and using a function 
                for old_t_days in range(registered_t.staying_period):
                    desired_date = i.arrival_date
                    for new_t_days in range(i.staying_period):
                        if desired_date == unaval_date and i.name!=registered_t.name:
                            raise ValidationError("The property is not available at that date")
                        desired_date += timedelta(days=1)
                    unaval_date += timedelta(days=1)

    # no_double_booking only checks that the arrival date of 2 transactions with the same property cannot be the same
    _sql_constraints = [
        ('no_double_book', 'unique(property, arrival_date)', 
        'You can\'t book that property on that night because it has already been reserved'),
        ('unique_name', 'unique(name)', 'That transaction number has already been used')
    ]
