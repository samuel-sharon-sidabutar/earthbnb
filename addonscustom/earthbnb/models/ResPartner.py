from odoo import api, fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'
    _description = 'New Description'
    
    is_owner = fields.Boolean(string='Is Owner')
    is_renter = fields.Boolean(string='Is Renter')
    
    properties = fields.One2many(comodel_name='earthbnb.property', inverse_name='owner', string='Owned Properties')
    transactions = fields.One2many(comodel_name='earthbnb.transaction', inverse_name='renter', string='Rent Transactions')
    
    