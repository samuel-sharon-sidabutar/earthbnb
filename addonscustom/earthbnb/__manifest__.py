# -*- coding: utf-8 -*-
{
    'name': "earthbnb",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'report_xlsx'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/menu.xml',
        'views/property_view.xml',
        'views/propertytype_view.xml',
        'views/renter_view.xml',
        'views/owner_view.xml',
        'views/staff_view.xml',
        'views/cleaner_view.xml',
        'views/chef_view.xml',
        'views/feature_view.xml',
        'views/transaction_view.xml',
        'report/report.xml',
        'report/print_property_pdf_report.xml',
        'wizzard/wageincrease_wizzard_view.xml'

    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
