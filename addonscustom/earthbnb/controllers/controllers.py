from odoo import http, models, fields
from odoo.http import request
import json

class Wikumart(http.Controller):
    @http.route('/earthbnb/getproperty/', auth='public', method=['GET'])
    def getProperty(self, **kw):
        properties = request.env['earthbnb.property'].search([])
        output = []
        for i in properties:
            output.append({
                'Property Address' : i.name,
                'Cost Per Night' : i.cost_per_night,
                'Description' : i.description,
                'Type': i.type.name
            })
        return json.dumps(output)


# -*- coding: utf-8 -*-
# from odoo import http


# class Earthbnb(http.Controller):
#     @http.route('/earthbnb/earthbnb', auth='public')
#     def index(self, **kw):
#         return "Hello, world"

#     @http.route('/earthbnb/earthbnb/objects', auth='public')
#     def list(self, **kw):
#         return http.request.render('earthbnb.listing', {
#             'root': '/earthbnb/earthbnb',
#             'objects': http.request.env['earthbnb.earthbnb'].search([]),
#         })

#     @http.route('/earthbnb/earthbnb/objects/<model("earthbnb.earthbnb"):obj>', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('earthbnb.object', {
#             'object': obj
#         })
