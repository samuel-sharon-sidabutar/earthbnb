from odoo import models, fields


class PartnerXlsx(models.AbstractModel):
    _name = 'report.earthbnb.report_property_xlsx'
    _inherit = 'report.report_xlsx.abstract'
    report_date = fields.Date.today()

    def generate_xlsx_report(self, workbook, data, property):
        # One sheet by partner
        sheet = workbook.add_worksheet('Property Listings')
        # Menambahkan informasi tanggal laporan
        sheet.write(0, 0, str(self.report_date))
        sheet.write(1, 0, 'Address')
        sheet.write(1, 1, 'Cost Per Night')
        sheet.write(1, 2, 'Description')
        sheet.write(1, 3, 'Type')
        sheet.write(1, 4, 'Owner')
        
        row = 2
        col = 0
        for obj in property:
            col = 0
            sheet.write(row, col, obj.name)
            sheet.write(row, col + 1, obj.cost_per_night)
            sheet.write(row, col + 2, obj.description)
            sheet.write(row, col + 3, obj.type.name)
            sheet.write(row, col + 4, obj.owner.name)

            for item in obj.features:
                sheet.write(row, col + 5, (str(item.ammount)+" "+item.name))
                col += 1
            row += 1